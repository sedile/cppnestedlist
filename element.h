#ifndef ELEMENT_H
#define ELEMENT_H

#include <iostream>
#include <string>

class Element {
protected:
    const std::string _id;
    const std::string _value;
public:
    explicit Element(std::string id, std::string value);
    virtual ~Element();

    virtual std::string getID() const;
    virtual std::string getValueAsString() const;
    virtual Element* getElement();
    virtual std::string getElementAsString() const;
};

#endif // ELEMENT_H
