#include "list.h"

List::List(std::string id, std::vector<Element*> values)
    : Element(id,""), _values(values) { }

List::~List() {
    for(Element *elem : _values){
        delete elem;
    }
}

std::vector<Element*> List::getListValues() const {
    return _values;
}

std::string List::getElementAsString() const {
    std::string liststring = "(";
    for(unsigned int i = 0; i < _values.size(); i++){
        std::string value = _values.at(i)->getElementAsString();
        size_t space = value.find(" ");
        value = value.erase(0, space + 1);
        value = value.erase(value.size() - 1, value.size());
        liststring.append(value + " ");
    }
    liststring.erase(liststring.size() - 1, liststring.size());
    liststring.append(")");

    return "(" + getID() + " " + liststring + ")";
}
