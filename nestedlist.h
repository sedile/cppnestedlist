#ifndef NESTEDLIST_H
#define NESTEDLIST_H

#include <vector>

#include "element.h"

class NestedList {
private:
    std::vector<Element*> *_nestedlist;
    void builtNestedList(std::vector<std::string> &content);
    std::vector<Element*> builtList(std::string &content);
public:
    explicit NestedList();
    virtual ~NestedList();

    void addElement(Element *element);
    bool parseNestedList(std::string &content);
    void exportNestedList(std::string path);
    std::string importNestedList(std::string path);
    Element* first();
    Element* second();
    Element* third();
    Element* fourth();
    Element* fifth();
    Element* sixth();
    Element* getElementByID(std::string id);
    unsigned int getSize();
    bool isEmpty();
    NestedList* getNestedList();
    std::string getNestedListAsString();
};

#endif // NESTEDLIST_H
