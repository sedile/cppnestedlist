#ifndef REAL_H
#define REAL_H

#include "element.h"

class Real: public Element {
public:
    explicit Real(std::string id, std::string value);
    virtual ~Real();

    float getValue() const;
    std::string getElementAsString() const override;
};

#endif // REAL_H
