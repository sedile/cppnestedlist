#include <iostream>

#include "nestedlist.h"
#include "integer.h"
#include "real.h"
#include "bool.h"
#include "text.h"
#include "list.h"

int main(){

    // Create an empty nested list //
    NestedList *nlist = new NestedList;

    // Create some elements to test //
    Element *twenty = new Integer("twenty","20");
    Element *heinrich = new Text("name","heinrich");

    std::vector<Element*> primlist;
    primlist.push_back(new Integer("1","2"));
    primlist.push_back(new Integer("2","3"));
    primlist.push_back(new Integer("3","5"));
    primlist.push_back(new Integer("4","7"));
    primlist.push_back(new Integer("5","11"));

    Element *prim = new List("primlist", primlist);

    nlist->addElement(twenty);
    nlist->addElement(prim);
    nlist->addElement(heinrich);
    nlist->addElement(new Real("euler","2.71"));

    // export/save and import/load the nested list //
    nlist->exportNestedList("/.../test.nl");
    std::string content = nlist->importNestedList("/.../test.nl");
    if (nlist->parseNestedList(content) ){

        // get one element example //
        std::cout << nlist->getNestedListAsString() << "\n";
        Element *element = nlist->getElementByID("euler");
        Real *pi = dynamic_cast<Real*>(element);
        float pi_number = pi->getValue();
        std::cout << pi_number << "\n";

        // list example (sum up prim numbers)
        Element *listelem = nlist->second();
        List *primlist = dynamic_cast<List*>(listelem);
        std::vector<Element*> vec_elems = primlist->getListValues();

        unsigned int sum = 0;
        for(unsigned short i = 0; i < vec_elems.size(); i++){
            Integer *x = dynamic_cast<Integer*>(vec_elems.at(i));
            sum += x->getValue();
        }
        std::cout << "prime sum : " << sum << "\n";
    }

    delete nlist;
    return 0;
}
