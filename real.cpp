#include "real.h"

Real::Real(std::string id, std::string value)
    : Element(id,value) { }

Real::~Real() { }

float Real::getValue() const {
    float value = std::stof(_value);
    return value;
}

std::string Real::getElementAsString() const {
    return "(" + getID() + " " + getValueAsString() + ")";
}
