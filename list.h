#ifndef LIST_H
#define LIST_H

#include <vector>

#include "element.h"

class List : public Element {
private:
    std::vector<Element*> _values;
public:
    explicit List(std::string id, std::vector<Element*> values);
    virtual ~List();

    std::vector<Element*> getListValues() const;
    std::string getElementAsString() const override;
};

#endif // LIST_H
