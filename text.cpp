#include "text.h"

Text::Text(std::string id, std::string value)
    : Element(id,value) { }

Text::~Text() { }

std::string Text::getValue() const {
    return _value;
}

std::string Text::getElementAsString() const {
    return "(" + getID() + " " + getValueAsString() + ")";
}
