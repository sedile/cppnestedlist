#include "bool.h"

Bool::Bool(std::string id, std::string value)
    : Element(id,value) { }

Bool::~Bool() { }

bool Bool::getValue() const {
    if (_value.compare("True") == 0 || _value.compare("true") == 0) {
        return true;
    } else {
        return false;
    }
}

std::string Bool::getElementAsString() const {
    return "(" + getID() + " " + getValueAsString() + ")";
}
