#include "element.h"

Element::Element(std::string id, std::string value)
    : _id(id), _value(value) { }

Element::~Element() { }

std::string Element::getID() const {
    return _id;
}

std::string Element::getValueAsString() const {
    return _value;
}

Element* Element::getElement() {
    return this;
}

std::string Element::getElementAsString() const {
    return "(" + getID() + " " + getValueAsString() + ")";
}
