#ifndef TEXT_H
#define TEXT_H

#include "element.h"

class Text: public Element {
public:
    explicit Text(std::string id, std::string value);
    virtual ~Text();

    std::string getValue() const;
    std::string getElementAsString() const override;
};

#endif // TEXT_H
