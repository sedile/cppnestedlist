#include "integer.h"

Integer::Integer(std::string id, std::string value)
    : Element(id,value) { }

Integer::~Integer() { }

int Integer::getValue() const {
    int value = std::stoi(_value);
    return value;
}

std::string Integer::getElementAsString() const {
    return "(" + getID() + " " + getValueAsString() + ")";
}
