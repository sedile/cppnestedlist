#include <fstream>

#include "nestedlist.h"
#include "list.h"
#include "real.h"
#include "integer.h"
#include "bool.h"
#include "text.h"

NestedList::NestedList() {
    _nestedlist = new std::vector<Element*>();
}

NestedList::~NestedList(){
    for(unsigned int i = 0; i < _nestedlist->size(); i++){
        delete _nestedlist->at(i);
    }
    delete _nestedlist;
}

bool NestedList::parseNestedList(std::string &content){
    std::vector<std::string> nlist_string;
    if ( content.at(0) != '(' ){
        return false;
    } else {
        unsigned int brackets = 0;
        unsigned int start = 0;
        for(unsigned int i = 1; i < content.size(); i++){
            if ( content.at(i) == '(' ){
                brackets++;
                if ( brackets == 1 ){
                    start = i;
                }
            } else if ( content.at(i) == ')' ){
                brackets--;
                if ( brackets == 0){
                    unsigned int end = i - start;
                    std::string element = content.substr(start,end+1);
                    nlist_string.push_back(element);
                }
            }
        }
    }
    builtNestedList(nlist_string);
    return true;
}

void NestedList::builtNestedList(std::vector<std::string> &elements){
    _nestedlist->clear();
    for(unsigned int i = 0; i < elements.size(); i++){
        std::string name;
        std::string value;
        std::string temp = elements.at(i);
        for(unsigned int j = 1; j < temp.size(); j++){
            if ( temp.at(j) == ' ' && temp.at(j+1) != '(' ){
                name = temp.substr(1,j-1);
                value = temp.substr(j+1, temp.size() - j - 2);

                if ( value.compare("true") == 0 || value.compare("True") == 0||
                     value.compare("false") == 0 || value.compare("False") == 0){
                    _nestedlist->push_back(new Bool(name,value));
                    break;
                } else {
                    try {
                        std::stof(value);
                        if ( value.find('.') > value.size() ){
                            _nestedlist->push_back(new Integer(name,value));
                        } else {
                            _nestedlist->push_back(new Real(name, value));
                        }
                        break;
                    } catch ( std::invalid_argument& ){
                        _nestedlist->push_back(new Text(name,value));
                        break;
                    }
                }
            } else if ( temp.at(j) == ' ' && temp.at(j+1) == '(' ){
                name = temp.substr(1,j-1);
                value = temp.substr(j+1, temp.size() - j - 2);
                Element *list = new List(name, builtList(value));
                _nestedlist->push_back(list);
                break;
            }
        }
    }
}

std::vector<Element*> NestedList::builtList(std::string &content){
    unsigned int start = 1;
    std::vector<Element*> values;
    for(unsigned int i = 1; i < content.size(); i++){
        if ( content.at(i) == ' ' || content.at(i) == ')' ){
            unsigned int end = i - start;
            std::string temp = content.substr(start,end);

            if ( temp.compare("true") == 0 || temp.compare("True") == 0||
                 temp.compare("false") == 0 || temp.compare("False") == 0){
                values.push_back(new Bool("",temp));
            } else {
                try {
                    std::stof(temp);
                    if ( temp.find('.') > temp.size() ){
                        values.push_back(new Integer("",temp));
                    } else {
                        values.push_back(new Real("", temp));
                    }
                } catch ( std::invalid_argument& ){
                    values.push_back(new Text("",temp));
                }
            }
            start = i + 1;
        }
    }
    return values;
}

void NestedList::exportNestedList(std::string path){
    std::ofstream file;
    file.open(path);
    file << getNestedListAsString();
    file.flush();
    file.close();
}

std::string NestedList::importNestedList(std::string path){
    std::string content;
    std::ifstream file(path);
    if ( file.is_open() ){
        std::string line;
        while(getline(file,line)){
            content.append(line);
        }
    }
    return content;
}

void NestedList::addElement(Element *element){
    _nestedlist->push_back(element);
}

Element* NestedList::first() {
    return _nestedlist->front();
}

Element* NestedList::second() {
    return _nestedlist->at(1);
}

Element* NestedList::third() {
    return _nestedlist->at(2);
}

Element* NestedList::fourth() {
    return _nestedlist->at(3);
}

Element* NestedList::fifth() {
    return _nestedlist->at(4);
}

Element* NestedList::sixth() {
    return _nestedlist->at(5);
}

Element* NestedList::getElementByID(std::string id){
    for(unsigned int i = 0; i < _nestedlist->size(); i++){
        if ( _nestedlist->at(i)->getID().compare(id) == 0 ){
            return _nestedlist->at(i);
        }
    }
    return new Element("null","null");
}

unsigned int NestedList::getSize() {
    return _nestedlist->size();
}

bool NestedList::isEmpty() {
    return _nestedlist->empty();
}

NestedList* NestedList::getNestedList(){
    return this;
}

std::string NestedList::getNestedListAsString(){
    std::string nlist = "(";
    for(unsigned int i = 0; i < _nestedlist->size(); i++){
        nlist.append(_nestedlist->at(i)->getElementAsString());
    }
    nlist.append(")");
    return nlist;
}
