#ifndef BOOL_H
#define BOOL_H

#include "element.h"

class Bool : public Element {
public:
    explicit Bool(std::string id, std::string value);
    virtual ~Bool();

    bool getValue() const;
    std::string getElementAsString() const override;
};

#endif // BOOL_H
