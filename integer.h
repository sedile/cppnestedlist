#ifndef INTEGER_H
#define INTEGER_H

#include "element.h"

class Integer: public Element {
public:
    explicit Integer(std::string id, std::string value);
    virtual ~Integer();

    int getValue() const;
    std::string getElementAsString() const override;
};
#endif // INTEGER_H
